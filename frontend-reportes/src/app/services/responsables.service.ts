import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service'

import { Observable, of } from 'rxjs';


import { map, catchError } from 'rxjs/operators';
import { Responsable } from '../shared/Responsable';

@Injectable({
  providedIn: 'root'
})
export class ResponsablesService {

  constructor(private http: HttpClient,
    private processHTTPMsgService: ProcessHttpmsgService) { }

  putResponsablebyId(id: string, datos:Responsable): Observable<Responsable> {
    return this.http.put<Responsable>(baseURL + 'Responsables/' + id, datos)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  
  getResponsablebyId(id: string): Observable<Responsable> {
    return this.http.get<Responsable>(baseURL + 'Responsables/' + id)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  deleteResponsablebyId(id: string): Observable<Responsable> {
    return this.http.delete<Responsable>(baseURL + 'Responsables/' + id.toString())
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  getResponsables(): Observable<Responsable[]> {
    return this.http.get<Responsable[]>(baseURL + 'Responsables')
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  postResponsables(monitor: Responsable): Observable<Responsable> {
    return this.http.post<Responsable>(baseURL + 'Responsables', monitor)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
