import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from './process-httpmsg.service'

import { Observable, of } from 'rxjs';


import { map, catchError } from 'rxjs/operators';
import { Actividad } from '../shared/Actividad';


@Injectable({
  providedIn: 'root'
})
export class ActividadesService {

  constructor(private http: HttpClient,
    private processHTTPMsgService: ProcessHttpmsgService) { }

  putActividadbyId(id: string, datos:Actividad): Observable<Actividad> {
    return this.http.put<Actividad>(baseURL + 'Actividades/' + id, datos)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  getActividadbyId(id: string): Observable<Actividad> {
    return this.http.get<Actividad>(baseURL + 'Actividades/' + id)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  deleteActividadbyId(id: string): Observable<Actividad> {
    return this.http.delete<Actividad>(baseURL + 'Actividades/' + id.toString())
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  getActividades(): Observable<Actividad[]> {
    return this.http.get<Actividad[]>(baseURL + 'Actividades')
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
  postActividades(monitor: Actividad): Observable<Actividad> {
    return this.http.post<Actividad>(baseURL + 'Actividades', monitor)
      .pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
