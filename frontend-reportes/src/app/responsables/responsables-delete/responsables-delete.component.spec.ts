import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsablesDeleteComponent } from './responsables-delete.component';

describe('ResponsablesDeleteComponent', () => {
  let component: ResponsablesDeleteComponent;
  let fixture: ComponentFixture<ResponsablesDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsablesDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsablesDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
