import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import 'hammerjs';

import { Router, ActivatedRoute } from '@angular/router';

import { FormControl, FormBuilder } from '@angular/forms';

import { Subscription } from 'rxjs';
import { Responsable } from 'src/app/shared/Responsable';
import { ResponsablesService } from 'src/app/services/responsables.service';


@Component({
  selector: 'app-responsables-delete',
  templateUrl: './responsables-delete.component.html',
  styleUrls: ['./responsables-delete.component.scss']
})
export class ResponsablesDeleteComponent implements OnInit {

  id: string;
  private sub: Subscription;
  data: Responsable | null;
  errMess: string;

  constructor(private fb: FormBuilder,
    private Monitores_Servicio: ResponsablesService,
    private route: ActivatedRoute,
    private router: Router,

    @Inject('BaseURL') private BaseURL) {
    this.sub = this.route.params.subscribe(params => {
      this.id = params._id;
      console.log(this.id)
    });
    if (this.id) {
      this.Monitores_Servicio.getResponsablebyId(this.id).subscribe(estudiante => {
        const estudianteEncontrado = estudiante;
        this.data = estudianteEncontrado;
        console.log(this.data);
      },
        errmess => this.errMess = <any>errmess);
    }
    else {
      this.router.navigate(['/Actas_de_Compromiso']);
    }

  }

  ngOnInit() {
  }
  Eliminar_Datos() {

    this.Monitores_Servicio.deleteResponsablebyId(this.id).subscribe(estudiante => {
      const estudianteEncontrado = estudiante;
      this.data = estudianteEncontrado;
      console.log(this.data);
    },
      errmess => this.errMess = <any>errmess);


  }
  Atras() {
    this.router.navigate(['/Responsables']);
  }


}
