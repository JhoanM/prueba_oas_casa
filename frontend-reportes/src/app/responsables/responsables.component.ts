import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import 'hammerjs';
import { SelectionModel } from '@angular/cdk/collections';

import { Router } from '@angular/router';


import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FormControl } from '@angular/forms';

import { ImgSrcDirective } from '@angular/flex-layout';
import { Responsable } from '../shared/Responsable';
import { ResponsablesService } from '../services/responsables.service';



@Component({
  selector: 'app-responsables',
  templateUrl: './responsables.component.html',
  styleUrls: ['./responsables.component.scss']
})
export class ResponsablesComponent implements OnInit {

  Monitores_Asignados: Responsable[] | null;

  SolicitudesRealizadas: Responsable[] | null;
  dataSource;
  codigo: String | null;
  errMess: string;
  data: Responsable | null;
  pagina_1;
  pagina_2;
  pagina_3;



  ApellidoFiltro = new FormControl('');
  
  NombreFiltro = new FormControl('');
  
  TelefonoFiltro = new FormControl('');
  EmailFiltro = new FormControl('');

  filterValues = {
    Apellidos: '',
    
    Nombres: '',
    Telefono: '',
    
    Email: ''
  }

  displayedColumns: string[] = ['Nombres', 'Apellidos', 'Telefono', 'Email', 'Acciones'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private Monitores_Servicio: ResponsablesService,
    private router: Router,

    @Inject('BaseURL') private BaseURL
  ) {
    this.Monitores_Servicio.getResponsables()
      .subscribe(solicitudes => {
        this.SolicitudesRealizadas = solicitudes;
        this.dataSource.data = solicitudes;
      },
        errmess => this.errMess = <any>errmess);
    this.dataSource = new MatTableDataSource<Responsable>(this.SolicitudesRealizadas);

    //this.SolicitudesRealizadas = TablaPrueba;
    //this.dataSource = new MatTableDataSource<Responsable>(this.SolicitudesRealizadas);

  }



  ngOnInit() {


    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.ApellidoFiltro.valueChanges
      .subscribe(
        codigo => {
          this.filterValues.Apellidos = codigo;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    
    this.NombreFiltro.valueChanges
      .subscribe(
        nombre => {
          this.filterValues.Nombres = nombre;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.TelefonoFiltro.valueChanges
      .subscribe(
        sala => {
          this.filterValues.Telefono = sala;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.EmailFiltro.valueChanges
      .subscribe(
        sala => {
          this.filterValues.Email = sala;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.dataSource.filterPredicate = this.createFilter();
  }


  createFilter() {
    const myFilterPredicate = function (data: Responsable, filter: string): boolean {
      let searchString = JSON.parse(filter);
      return data.Nombres.toString().trim().indexOf(searchString.Nombre) !== -1
        && data.Apellidos.toString().trim().indexOf(searchString.Documento) !== -1
        && data.Telefono.toString().trim().indexOf(searchString.Telefono) !== -1
        && data.Email.toString().trim().indexOf(searchString.Email) !== -1

    }
    return myFilterPredicate;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  Modificar_Datos(datos: any) {
    this.router.navigate(['/Editar_Responsables', { _id: datos._id }])


  }
  Agregar_Datos() {

    this.router.navigate(['/Agregar_Responsables'])

  }
  Eliminar_Datos(datos: any) {
    this.router.navigate(['/Eliminar_Responsables', { _id: datos._id }])

  }



}
