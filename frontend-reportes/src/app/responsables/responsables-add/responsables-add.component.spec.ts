import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsablesAddComponent } from './responsables-add.component';

describe('ResponsablesAddComponent', () => {
  let component: ResponsablesAddComponent;
  let fixture: ComponentFixture<ResponsablesAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsablesAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsablesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
