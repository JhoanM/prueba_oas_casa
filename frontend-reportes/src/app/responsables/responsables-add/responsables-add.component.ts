import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { MatDialog, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
import { ActividadesService } from '../../services/actividades.service';
import { Responsable } from '../../shared/Responsable';
import { ResponsablesService } from '../../services/responsables.service';


export interface type_D {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-responsables-add',
  templateUrl: './responsables-add.component.html',
  styleUrls: ['./responsables-add.component.scss']
})
export class ResponsablesAddComponent implements OnInit {

  selected: any;
  errMess: string;
  estudiante: Responsable;
  carga_agregada: boolean = false;
  private sub: Subscription;

  

  formErrors = {
    'Nombres': '',
    'Apellidos': '',
    'Telefono': '',
    'Email': '',
  };

  validationMessages = {
    'Nombres': {
      'required': 'El Nombre es requerido.',
      
    },
    'Apellidos': {
      'required': 'El Apellido es requerido'
    },
    
    'Telefono': {
      'required': 'Se requiere numero de Telefono Celular',
      'pattern': 'El telefono solo debe contener numeros'
    },
    'Email': {
      'required': 'Se requiere correo electronico',
      'email': 'Formato de correo no valido'
    },
  };


  @ViewChild('fform') AsignacionCargaFormDirective;
  

  AsignacionCargaForm: FormGroup;

  Carga_monitor: Responsable;


  constructor(private fb: FormBuilder,
    private Responsables_Servicio: ResponsablesService,
    private route: ActivatedRoute,
    private router: Router,

    @Inject('BaseURL') private BaseURL) {
    this.createForm();

  }

  ngOnInit() {

  }

  createForm() {
    this.AsignacionCargaForm = this.fb.group({
      Nombres: ['', [Validators.required]],

      Apellidos: ['', [Validators.required]],
      
      Telefono: ['', [Validators.required, Validators.pattern]],
      Email: ['', [Validators.email, Validators.required]],
      
    });
    
    this.AsignacionCargaForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
    //this.sub = this.route.params.subscribe(params => {
      //this.AsignacionCargaForm.setValue(params);
      //this.EstudianteSeleccionado = params;
    //});

  }

  onSubmit() {

    this.Carga_monitor = this.AsignacionCargaForm.value;

    console.log(this.Carga_monitor);

    this.Responsables_Servicio.postResponsables(this.Carga_monitor)
      .subscribe(estudiante => {
        const estudianteEncontrado = estudiante;
        this.estudiante = estudianteEncontrado[0];
      },
        errmess => this.errMess = <any>errmess);

    this.AsignacionCargaForm.reset({
      Nombres: '',
      Apellidos: '',
      Telefono: '',
      Email: ''

    });
    this.AsignacionCargaFormDirective.resetForm();
  }
  onValueChanged(data?: any) {
    if (!this.AsignacionCargaForm) { return; }
    const form = this.AsignacionCargaForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }

  }
  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }
    else {
      return value;
    }
  }
  Atras() {
    this.router.navigate(['/Actividades']);
  }

}
