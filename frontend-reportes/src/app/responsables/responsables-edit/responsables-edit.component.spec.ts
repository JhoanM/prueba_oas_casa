import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsablesEditComponent } from './responsables-edit.component';

describe('ResponsablesEditComponent', () => {
  let component: ResponsablesEditComponent;
  let fixture: ComponentFixture<ResponsablesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsablesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsablesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
