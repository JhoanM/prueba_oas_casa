import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import 'hammerjs';
import { SelectionModel } from '@angular/cdk/collections';

import { Router } from '@angular/router';


import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FormControl } from '@angular/forms';

import { ImgSrcDirective } from '@angular/flex-layout';
import { Responsable } from '../shared/Responsable';
import { ResponsablesService } from '../services/responsables.service';
import { Actividad } from '../shared/Actividad';
import { ActividadesService } from '../services/actividades.service';


@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.scss']
})
export class ActividadesComponent implements OnInit {

  Monitores_Asignados: Actividad[] | null;

  SolicitudesRealizadas: Actividad[] | null;
  dataSource;
  codigo: String | null;
  errMess: string;
  data: Responsable | null;
  pagina_1;
  pagina_2;
  pagina_3;



  Fecha_CreacionFiltro = new FormControl('');
  
  Fecha_LimiteFiltro = new FormControl('');
  EstadoFiltro = new FormControl('');
  ResponsableFiltro = new FormControl('');
  DescripcionFiltro = new FormControl('');

  filterValues = {
    Fecha_Creacion: '',
    
    Fecha_Limite: '',
    Responsable: '',
    
    Descripcion: '',
    Estado:''
  }

  displayedColumns: string[] = ['Fecha_Creacion', 'Fecha_Limite', 'Responsable', 'Descripcion', 'Acciones'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private Monitores_Servicio: ActividadesService,
    private router: Router,

    @Inject('BaseURL') private BaseURL
  ) {
    this.Monitores_Servicio.getActividades()
      .subscribe(solicitudes => {
        this.SolicitudesRealizadas = solicitudes;
        this.dataSource.data = solicitudes;
      },
        errmess => this.errMess = <any>errmess);
    this.dataSource = new MatTableDataSource<Actividad>(this.SolicitudesRealizadas);

    //this.SolicitudesRealizadas = TablaPrueba;
    //this.dataSource = new MatTableDataSource<Responsable>(this.SolicitudesRealizadas);

  }



  ngOnInit() {


    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.Fecha_CreacionFiltro.valueChanges
      .subscribe(
        codigo => {
          this.filterValues.Fecha_Limite = codigo;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    
    this.Fecha_LimiteFiltro.valueChanges
      .subscribe(
        nombre => {
          this.filterValues.Fecha_Limite = nombre;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.ResponsableFiltro.valueChanges
      .subscribe(
        sala => {
          this.filterValues.Responsable = sala;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.DescripcionFiltro.valueChanges
      .subscribe(
        sala => {
          this.filterValues.Descripcion = sala;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
      this.EstadoFiltro.valueChanges
      .subscribe(
        sala => {
          this.filterValues.Estado = sala;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )
    this.dataSource.filterPredicate = this.createFilter();
  }


  createFilter() {
    const myFilterPredicate = function (data: Actividad, filter: string): boolean {
      let searchString = JSON.parse(filter);
      return data.Fecha_Creacion.toString().trim().indexOf(searchString.Fecha_Creacion) !== -1
        && data.Fecha_Limite.toString().trim().indexOf(searchString.Fecha_Limite) !== -1
        && data.Responsable.toString().trim().indexOf(searchString.Responsable) !== -1
        && data.Descripcion.toString().trim().indexOf(searchString.Descripcion) !== -1
        && data.Estado.toString().trim().indexOf(searchString.Estado) !== -1
        

    }
    return myFilterPredicate;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  Modificar_Datos(datos: any) {
    this.router.navigate(['/Editar_Actividades', { _id: datos._id }])


  }
  Agregar_Datos() {

    this.router.navigate(['/Agregar_Actividades'])

  }
  Eliminar_Datos(datos: any) {
    this.router.navigate(['/Eliminar_Actividades', { _id: datos._id }])

  }


}
