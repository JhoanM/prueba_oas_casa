import { Component, OnInit, Inject, ViewChild, Output, EventEmitter } from '@angular/core';
import 'hammerjs';

import { Router, ActivatedRoute } from '@angular/router';

import { FormControl, FormBuilder } from '@angular/forms';

import { Subscription } from 'rxjs';
import { Responsable } from 'src/app/shared/Responsable';
import { ResponsablesService } from 'src/app/services/responsables.service';
import { ActividadesService } from 'src/app/services/actividades.service';
import { Actividad } from 'src/app/shared/Actividad';
@Component({
  selector: 'app-actividades-delete',
  templateUrl: './actividades-delete.component.html',
  styleUrls: ['./actividades-delete.component.scss']
})
export class ActividadesDeleteComponent implements OnInit {

  id: string;
  private sub: Subscription;
  data: Actividad | null;
  errMess: string;

  constructor(private fb: FormBuilder,
    private Monitores_Servicio: ActividadesService,
    private route: ActivatedRoute,
    private router: Router,

    @Inject('BaseURL') private BaseURL) {
    this.sub = this.route.params.subscribe(params => {
      this.id = params._id;
      console.log(this.id)
    });
    if (this.id) {
      this.Monitores_Servicio.getActividadbyId(this.id).subscribe(estudiante => {
        const estudianteEncontrado = estudiante;
        this.data = estudianteEncontrado;
        console.log(this.data);
      },
        errmess => this.errMess = <any>errmess);
    }
    else {
      this.router.navigate(['/Actas_de_Compromiso']);
    }

  }

  ngOnInit() {
  }
  Eliminar_Datos() {

    this.Monitores_Servicio.deleteActividadbyId(this.id).subscribe(estudiante => {
      const estudianteEncontrado = estudiante;
      this.data = estudianteEncontrado;
      console.log(this.data);
    },
      errmess => this.errMess = <any>errmess);


  }
  Atras() {
    this.router.navigate(['/Actividades']);
  }


}
