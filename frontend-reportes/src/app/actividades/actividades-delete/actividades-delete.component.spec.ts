import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActividadesDeleteComponent } from './actividades-delete.component';

describe('ActividadesDeleteComponent', () => {
  let component: ActividadesDeleteComponent;
  let fixture: ComponentFixture<ActividadesDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadesDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActividadesDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
