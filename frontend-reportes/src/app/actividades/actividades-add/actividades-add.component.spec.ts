import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActividadesAddComponent } from './actividades-add.component';

describe('ActividadesAddComponent', () => {
  let component: ActividadesAddComponent;
  let fixture: ComponentFixture<ActividadesAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadesAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActividadesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
