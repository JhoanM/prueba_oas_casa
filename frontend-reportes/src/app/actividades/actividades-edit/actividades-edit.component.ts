import { Component, OnInit, ViewChild, Inject, } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

import { MatDialog, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';
import { ActividadesService } from '../../services/actividades.service';
import { Responsable } from '../../shared/Responsable';
import { Actividad } from 'src/app/shared/Actividad';
import { ResponsablesService } from 'src/app/services/responsables.service';


@Component({
  selector: 'app-actividades-edit',
  templateUrl: './actividades-edit.component.html',
  styleUrls: ['./actividades-edit.component.scss']
})
export class ActividadesEditComponent implements OnInit {

  selected: any;
  Responsable: Responsable[]|null;
  id:string
  errMess: string;
  estudiante: Actividad;
  carga_agregada: boolean = false;
  private sub: Subscription;

  

  formErrors = {
    'Fecha_Creacion': '',
    'Fecha_Limite': '',
    'Responsable': '',
    'Descripcion': '',
    'Estado':''
  };

  validationMessages = {
    'Fecha_Creacion': {
      'required': 'Fecha de Creacion requerida',
      
    },
    'Fecha_Limite': {
      'required': 'Fecha limite requerida'
    },
    
    'Responsable': {
      'required': 'Se requiere Responsable',
      'pattern': 'El telefono solo debe contener numeros'
    },
    'Descripcion': {
      'required': 'Se requiere Descripcion'
    },
    'Estado': {
      'required': 'Se requiere Estado'
    },
  };


  @ViewChild('fform') AsignacionCargaFormDirective;
  

  AsignacionCargaForm: FormGroup;

  Carga_monitor: Actividad;


  constructor(private fb: FormBuilder,
    private Responsables_Servicio: ActividadesService,
    private Responsables_Servicio2: ResponsablesService,
    private route: ActivatedRoute,
    private router: Router,

    @Inject('BaseURL') private BaseURL) {
      this.sub = this.route.params.subscribe(params => {
        this.id = params._id;
        console.log(this.id)
      });
      if (this.id){
        this.Responsables_Servicio.getActividadbyId(this.id).subscribe(estudiante => {
          const estudianteEncontrado = estudiante;
          this.estudiante = estudianteEncontrado;
          console.log(this.estudiante);
          this.createForm();
        },
          errmess => this.errMess = <any>errmess);
      }
      else{
        this.router.navigate(['/Actividades']);
      }
      this.Responsables_Servicio2.getResponsables()
      .subscribe(solicitudes => {
        this.Responsable = solicitudes;
      },
        errmess => this.errMess = <any>errmess);



  }

  ngOnInit() {

  }

  createForm() {
    this.AsignacionCargaForm = this.fb.group({
      Fecha_Creacion: [this.estudiante.Fecha_Creacion, [Validators.required]],

      Fecha_Limite: [this.estudiante.Fecha_Limite, [Validators.required]],
      
      Responsable: [this.estudiante.Responsable, [Validators.required]],
      Descripcion: [this.estudiante.Descripcion, [Validators.required]],
      Estado:[this.estudiante.Estado,[Validators.required]]
      
    });
    
    this.AsignacionCargaForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
    //this.sub = this.route.params.subscribe(params => {
      //this.AsignacionCargaForm.setValue(params);
      //this.EstudianteSeleccionado = params;
    //});

  }

  onSubmit() {

    this.Carga_monitor = this.AsignacionCargaForm.value;

    console.log(this.Carga_monitor);

    this.Responsables_Servicio.putActividadbyId(this.id,this.Carga_monitor)
      .subscribe(estudiante => {
        const estudianteEncontrado = estudiante;
        this.estudiante = estudianteEncontrado[0];
      },
        errmess => this.errMess = <any>errmess);

    this.AsignacionCargaForm.reset({
      Fecha_Creacion: '',
      Fecha_Limite: '',
      Responsable: '',
      Descripcion: ''

    });
    this.AsignacionCargaFormDirective.resetForm();
  }
  onValueChanged(data?: any) {
    if (!this.AsignacionCargaForm) { return; }
    const form = this.AsignacionCargaForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }

  }
  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }
    else {
      return value;
    }
  }
  Atras() {
    this.router.navigate(['/Actividades']);
  }


}
