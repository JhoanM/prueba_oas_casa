import { Routes } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { ActividadesComponent } from '../actividades/actividades.component';
import { ReportesComponent } from '../reportes/reportes.component';
import { ResponsablesComponent } from '../responsables/responsables.component';
import { ResponsablesAddComponent } from '../responsables/responsables-add/responsables-add.component';
import { ResponsablesEditComponent } from '../responsables/responsables-edit/responsables-edit.component';
import { ResponsablesDeleteComponent } from '../responsables/responsables-delete/responsables-delete.component';
import { ActividadesAddComponent } from '../actividades/actividades-add/actividades-add.component';
import { ActividadesEditComponent } from '../actividades/actividades-edit/actividades-edit.component';
import { ActividadesDeleteComponent } from '../actividades/actividades-delete/actividades-delete.component';


export const routes: Routes = [
  { path: 'Home', component: HomeComponent },
  { path: 'Actividades', component: ActividadesComponent },
  { path: 'Agregar_Actividades', component: ActividadesAddComponent },
  { path: 'Editar_Actividades', component: ActividadesEditComponent },
  { path: 'Eliminar_Actividades', component: ActividadesDeleteComponent },
  { path: 'Responsables', component: ResponsablesComponent },
  { path: 'Agregar_Responsables', component: ResponsablesAddComponent },
  { path: 'Editar_Responsables', component: ResponsablesEditComponent },
  { path: 'Eliminar_Responsables', component: ResponsablesDeleteComponent },
  { path: 'Reportes', component: ReportesComponent },
  { path: '**', component: HomeComponent },
  
  { path: '', redirectTo: '/Home', pathMatch: 'full' }
];