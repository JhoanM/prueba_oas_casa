//Modulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { ActividadesEditComponent } from './actividades/actividades-edit/actividades-edit.component';
import { ActividadesDeleteComponent } from './actividades/actividades-delete/actividades-delete.component';
import { ResponsablesComponent } from './responsables/responsables.component';
import { ResponsablesEditComponent } from './responsables/responsables-edit/responsables-edit.component';
import { ResponsablesDeleteComponent } from './responsables/responsables-delete/responsables-delete.component';
import { ReportesComponent } from './reportes/reportes.component';

//Servicios

import { ProcessHttpmsgService } from './services/process-httpmsg.service';

//Variables

import { baseURL } from './shared/baseurl';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { ResponsablesAddComponent } from './responsables/responsables-add/responsables-add.component';
import { ActividadesAddComponent } from './actividades/actividades-add/actividades-add.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ActividadesComponent,
    ActividadesEditComponent,
    ActividadesDeleteComponent,
    ResponsablesComponent,
    ResponsablesEditComponent,
    ResponsablesDeleteComponent,
    ReportesComponent,
    ResponsablesAddComponent,
    ActividadesAddComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatListModule,
    MatSidenavModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatDialogModule,
    FlexLayoutModule,
    MatSelectModule,
    MatPaginatorModule,
    HttpModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ProcessHttpmsgService,
    { provide: 'BaseURL', useValue: baseURL }],
  bootstrap: [AppComponent]
})
export class AppModule { }
