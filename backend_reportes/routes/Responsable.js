const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const Responsables = require('../models/Responsable');

const ResponsableRouter = express.Router();
ResponsableRouter.use(bodyParser.json());

ResponsableRouter.route('/').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Responsables.find({}).sort({Nombre: -1})
            .then((responsables) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(responsables);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
        Responsables.create(req.body)
            .then((responsable) => {
                console.log('Responsable Created', responsable);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(responsable);

            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion PUT no es soportada para esta ruta');
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion DELETE no es soportada para esta ruta');
    });

ResponsableRouter.route('/:responsableId').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Responsables.findById(req.params.responsableId)
            .then((responsables) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(responsables);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions, 
        (req, res, next) => {

    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        Responsables.findByIdAndUpdate(req.params.responsableId, {
            $set: req.body
        }, { new: true })
            .then((responsable) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(responsable);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        Responsables.findByIdAndRemove(req.params.responsableId)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });



module.exports = ResponsableRouter;
