const mongoose = require('mongoose');
const Schemma = mongoose.Schema;


const ResponsableSchema = new Schemma({
    
    Nombres: {
        type: String,
        required: true
    },
    Apellidos: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true
    },
    Telefono: {
        type: String,
        required: true
    },
    
}, {
        timestamps: false
    });


var Responsables = mongoose.model('Responsable', ResponsableSchema);

module.exports = Responsables;
