const mongoose = require('mongoose');
const Schemma = mongoose.Schema;


const ActividadSchema = new Schemma({

    Fecha_Creacion:{
        type: String,
        required: true
    },
    Fecha_Limite: {
        type: String,
        required: true
    },
    Responsable: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Responsable',
        required: true
    },
    Descripcion: {
        type: String,
        required: true
    },
    Estado: {
        type: String,
        required: true
    },
    
}, {
        timestamps: true
    });



var Actividades = mongoose.model('Actividad', ActividadSchema);

module.exports = Actividades;
